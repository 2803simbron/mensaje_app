/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mensajes_app;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author adolfo
 */
public class MensajesDAO {

    public static void crearMensajeB(Mensajes mensaje) {
        Conexion db_connect = new Conexion();

        try ( Connection conexion = db_connect.get_connection()) {
            PreparedStatement ps = null;
            try {
                String consulta = "INSERT INTO `mensajes` ( `mensaje`, `autor_mensaje`) VALUES (?,?)";
                ps = conexion.prepareStatement(consulta);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println("mensaje Creado con Exito");

            } catch (SQLException ex) {
                System.out.println(ex);

            }

        } catch (SQLException e) {
            System.out.println(e);

        }

    }

    public static void leerMensajesDB() {
        Conexion db_connect = new Conexion();
        PreparedStatement ps = null;
        ResultSet rs = null;

        try ( Connection conexion = db_connect.get_connection()) {
            String consulta = "SELECT * FROM mensajes";
            ps = conexion.prepareStatement(consulta);
            rs = ps.executeQuery();

            while (rs.next()) {
                System.out.println("Id: " + rs.getInt("id_mensaje"));
                System.out.println("Mensaje: " + rs.getString("mensaje"));
                System.out.println("Autor: " + rs.getString("autor_mensaje"));
                System.out.println("Fecha: " + rs.getString("fecha_mensaje"));
                System.out.println("");
            }

        } catch (SQLException ex) {
            System.out.println("No Se ha podido recuperar los mensajes ");
            System.out.println(ex);

        }

    }

    public static void borrarMensajesDB(int id_mensaje) {
        Conexion db_connect = new Conexion();
        try ( Connection conexion = db_connect.get_connection()) {
            PreparedStatement ps = null;
                    try {
                       String consulta = "DELETE FROM mensajes WHERE id_mensaje = ?";
                       ps = conexion.prepareStatement(consulta);
                       ps.setInt(1, id_mensaje);
                       ps.executeUpdate();
                        System.out.println("El mensaje ha sido borrado con Exito");
                    }catch(SQLException e){
                        System.out.println("No Se ha podido borrar el mensaje ");
                        System.out.println(e);
                    }

        } catch (SQLException ex) {
            System.out.println(ex);
        }

    }

    public static void actualizarMensajesDB(Mensajes mensaje) {
         Conexion db_connect = new Conexion();
         try ( Connection conexion = db_connect.get_connection()){
             PreparedStatement ps = null;
                    try{
                        String consulta = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?";
                        ps = conexion.prepareStatement(consulta);
                        ps.setString(1, mensaje.getMensaje());
                        ps.setInt(2, mensaje.getId_mensaje());
                        ps.executeUpdate();
                        System.out.println("Mensaje se Actualizo correctamente");
                        
                        
                    }catch(SQLException ex){
                        System.out.println(ex);
                        System.out.println("No se ha podido Actualizar Mensaje");
                        
                    }
             
             
         }catch(SQLException e){
                       
                        System.out.println(e);
                    }

    }

}
